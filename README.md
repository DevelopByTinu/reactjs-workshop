React.js Workshop
=================

This runs the webpack dev server, any changes you make to javascript
files in `excercises` will cause the browser to reload live.

Links mentioned in the workshop
-------------------------------

https://github.com/ryanflorence/react-training/blob/gh-pages/lessons/05-wrapping-dom-libs.md

http://react-router-mega-demo.herokuapp.com/
